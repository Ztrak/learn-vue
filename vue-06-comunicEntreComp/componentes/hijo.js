Vue.component('hijo',{
    template: `
        <div  class="py-5 bg-danger">
            <h3>Componente Hijo: {{numero}}</h3>
            <h4>Nombre: {{nombre}}</h4>
        </div> 
    `,
    props: ['numero'],
    /*los props son arrays permiten hacer una comunicación entre
    los componentes
     */
    data(){
        return{
            nombre:'Ignacio'
        }
    },
    mounted(){
        /*se activa después de que se ejecute el DOM, o sea cuando la data
          haya sido leída, proviene del ciclo de vida de Vue */
        this.$emit('nombreHijo',this.nombre) //emitiendo un evento
    }

})