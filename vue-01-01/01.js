//instanciar Vue
const app = new Vue({
    //recibiendo un objeto
    //dentro de estas llaves vamos a estar trabajando con vue
    //estamos llamando a toda la libreria del script de Vue
    el: '#app', //detectando el id; dentro de app, o sea del contenido, va a estar gestionado por Vue
    // el y data son propios de Vue
    //en data abrimos otro objeto
    data: {
        //trabajando con los datos
        titulo: 'Hola mundo con Vue',
        frutas: ['naranja','platano','papaya'],
        menestras: [
            {nombre: 'lentejas',cantidad: 12},
            {nombre: 'frejoles',cantidad: 0},
            {nombre: 'alverjitas',cantidad: 7}
        ],
        nuevaFruta : '',
        total: 0
    },
    methods:{
        agregarFruta() {
            this.menestras.push({
                nombre: this.nuevaFruta, cantidad: 0
            });
            this.nuevaFruta = '';
        }
    },
    computed: { //son métodos especiales
        sumarMenestras(){
            this.total = 0;
            for (menes of this.menestras){
                this.total = this.total + menes.cantidad
            }
            return this.total
        }
    }
})