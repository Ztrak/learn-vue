const app = new Vue({
    el: '#app',
    data:{
        titulo: 'GYM con vue',
        tareas:[],
        nuevaTarea: ''
    },
    methods: {
        agregarTarea() {
            //console.log('diste click', this.nuevaTarea)
            this.tareas.push({
                nombre: this.nuevaTarea,
                estado: false
            })
            //console.log(this.tareas)
            this.nuevaTarea=''
        },
        editarTarea(index){
            this.tareas[index].estado=true
        },
        eliminar(index){
            this.tareas.splice(index,1)
        }
    }
})