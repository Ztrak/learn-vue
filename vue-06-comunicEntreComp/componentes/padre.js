Vue.component('padre',{
    template: `
        <div class="p-5 bg-dark text-white">
            <h1>Componente Padre: {{contador}}</h1>
            <button @click="contador++">+</button>
            <!--Desde el componente padre mandando algún dato al
            componente hijo-->
            {{nombrePadre}}
            <hijo :numero="contador" @nombreHijo="nombrePadre=$event"></hijo>
        </div>
    `,
    data(){
        return{
            contador: 0,
            nombrePadre:''
        }
    }
})