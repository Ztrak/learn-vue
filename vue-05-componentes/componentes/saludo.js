/*Creando el primer componente, el primer parámetro entre comillas simples debe
        tener el mismo nombre que la etiqueta creada en el div exclusivo para Vue*/

Vue.component('saludo',{

    //el componente debe tener siempre un template para realizar las etiquetas HTML
    template:`
                <div>
                    <h1>{{saludo}}</h1>
                    <h3>gaaaaa</h3>
                </div>
            `,
    data(){
        return{
            saludo:'Hola desde Vue'
        }
    }

})